/* jshint esversion: 6 */
/* jshint -W097 */
'use strict';

// Возвращает случайное число между min (включительно) и max (не включая max)
function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

class Vector {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    plus(v) {
        if (!(v instanceof Vector)) {
            throw new Error("Vector.Plus(): \'v\' must be instance of Vector.");
        }
        return new Vector(this.x + v.x, this.y + v.y);
    }

    times(n = 1) {
        return new Vector(this.x*n, this.y*n);
    }
}

class Actor {
    constructor(pos = new Vector(), size = new Vector(1, 1), speed = new Vector()) {
        if (!(pos instanceof Vector)) {
            throw new Error("Actor: \'pos\' must be instance of Vector.");
        }
        if (!(size instanceof Vector)) {
            throw new Error("Actor: \'size\' must be instance of Vector.");
        }
        if (!(speed instanceof Vector)) {
            throw new Error("Actor: \'speed\' must be instance of Vector.");
        }
        this.pos = pos;
        this.size = size;
        this.speed = speed;
    }

    get left() { return this.pos.x; }
    get right() { return this.pos.x + this.size.x; }
    get top() { return this.pos.y; }
    get bottom() { return this.pos.y + this.size.y; }
    get type() { return "actor"; }

    act() {}

    isIntersect(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error("Actor.isIntersect(): \'actor\' must be instance of Actor.");
        }
        // сам с собой не пересекается
        if (this == actor) {
            return false;
        }
        return (actor.right > this.left && actor.left < this.right ) &&
                (actor.bottom > this.top && actor.top < this.bottom);
    }
}

class Level {
    constructor(grid = new Array(0), actors = new Array(0)) {
        this.grid = grid.slice();
        this.actors = actors.slice();
        this.player = actors.find(el => el.type == "player");
        this.status = null;
        this.height = Array.isArray(this.grid) ? this.grid.length : 0;
        this.width = 0;
        // за ширину уровня берем максимальную из длин вложенных массивов сетки 
        if (this.height > 0) {
            this.width = this.grid.reduce((memo, el) => {
                if (Array.isArray(el) && el.length > memo) {
                    memo = el.length;
                }
                return memo;
            }, 0);
        }
        this.finishDelay = 1;
    }

    isFinished() {
        return (this.status !== null && this.finishDelay < 0);
    }

    actorAt(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error("Level.actorAt(): \'actor\' must be instance of Actor.");
        }
        return this.actors.find(el => el.isIntersect(actor));
    }

    obstacleAt(pos, size) {
        if (!(pos instanceof Vector)) {
            throw new Error("Level.obstacleAt(): \'pos\' must be instance of Vector.");
        }
        if (!(size instanceof Vector)) {
            throw new Error("Level.obstacleAt(): \'size\' must be instance of Vector.");
        }

        // проверяем на выход за левую, правую и верхнюю границы уровня
        if (pos.x < 0 || pos.x + size.x > this.width || pos.y < 0) {
            return "wall";
        }
        // проверяем на выход за нижнюю границу уровня
        if (pos.y + size.y > this.height) {
            return "lava";
        }

        // Округляем проверяемые границы, поскольку препятствия имеют 
        // целочисленные координаты и размеры
        let xLeft = Math.floor(pos.x);
        let xRight = Math.floor(pos.x + size.x);
        let yTop = Math.floor(pos.y);
        let yBottom = Math.floor(pos.y + size.y);

        // Проверяем ячейки сетки по верхней и нижней границам
        for (let j = xLeft; j <= xRight; j++) {
            if (this.grid[yTop][j] !== undefined) {
                return this.grid[yTop][j];
            }
            if (this.grid[yBottom][j] !== undefined) {
                return this.grid[yBottom][j];
            }
        }

        // Проверяем ячейки сетки по левой и правой границам
        for (let i = yTop; i <= yBottom; i++) {
            if (this.grid[i][xLeft] !== undefined) {
                return this.grid[i][xLeft];
            }
            if (this.grid[i][xRight] !== undefined) {
                return this.grid[i][xRight];
            }
        }
    }

    removeActor(actor) {
        let actorIndex = this.actors.indexOf(actor);
        if (actorIndex > -1) {
            this.actors.splice(actorIndex, 1);
        }
    }

    noMoreActors(type) {
        return !(this.actors.some(el => el.type === type));
    }

    playerTouched(type, actor) {
        // Игра уже завершилась
        if (this.status !== null) {
            return;
        }

        // Проигрыш
        if (type === "lava" || type === "fireball") {
            this.status = "lost";
        // Собрали монету
        } else if (type === "coin") {
            this.removeActor(actor);
            // Победа
            if (this.noMoreActors("coin")) {
                this.status = "won";
            }
        }
    }
}

class LevelParser {
    constructor(dict) {
        this.dict = (dict) ? dict : Object.create(null);    
    }

    actorFromSymbol(symbol) {
        if (symbol && symbol in this.dict) {
            return this.dict[symbol];
        } else {
            return undefined;
        }
    }

    obstacleFromSymbol(symbol) {
        switch(symbol) {
            case "x":
                return "wall";
            case "!":
                return "lava";
            default:
                return undefined;
        }
    }

    createGrid(strings) {
        return strings.map(el => el.split("").map(subEl => this.obstacleFromSymbol(subEl)));
    }

    createActors(strings) {
        if (Array.isArray(strings)) {
            return strings.reduce( (memo, el, i) => {
                el.split("").forEach( (subEl, j) => {
                    let actor = this.actorFromSymbol(subEl);
                    if (actor && (actor === Actor || Actor.prototype.isPrototypeOf(actor.prototype))) {
                        memo.push(new actor(new Vector(j, i)));
                    }
                });
                return memo;
            }, []);    
        } else {
            return [];    
        }
    }

    parse(strings) {
        return new Level(this.createGrid(strings), this.createActors(strings));
    }
}

class Fireball extends Actor {
    constructor(pos = new Vector(), speed = new Vector()) {
        super(pos, new Vector(1, 1), speed);
    }

    get type() { return "fireball"; }

    getNextPosition(time) {
        return this.pos.plus(this.speed.times(time));
    }

    handleObstacle() {
        this.speed = this.speed.times(-1);
    }

    act(time, level) {
        // Предполагаемая следующая позиция актора
        let nextPos = this.getNextPosition(time);
        // Обрабатываем столкновение с препятствием в следующей позиции
        if (level.obstacleAt(nextPos, this.size)) {
            this.handleObstacle();
        // или продолжаем движение
        } else {
            this.pos = nextPos;
        }
    }
}

class HorizontalFireball extends Fireball {
    constructor(pos = new Vector()) {
        super(pos, new Vector(2, 0));
    }
}

class VerticalFireball extends Fireball {
    constructor(pos = new Vector()) {
        super(pos, new Vector(0, 2));
    }    
}

class FireRain extends Fireball {
    constructor(pos = new Vector()) {
        super(pos, new Vector(0, 3));
        this.initPos = pos;
    }

    // Огненный дождь должен стартовать с начальной позиции
    handleObstacle() {
        this.pos = this.initPos;
    }
}

class Coin extends Actor {
    constructor(pos = new Vector()) {
        super(pos.plus(new Vector(0.2, 0.1)), new Vector(0.6, 0.6));
        this.initPos = this.pos;

        // Свойства для реализации подпрыгивания
        this.springSpeed = 8;
        this.springDist = 0.07;
        this.spring = getRandom(0, 2 * Math.PI);
    }

    get type() { return "coin"; }

    updateSpring(time = 1) {
        this.spring += time * this.springSpeed;
    }

    getSpringVector() {
        return new Vector(0, Math.sin(this.spring) * this.springDist);
    }

    getNextPosition(time = 1) {
        this.updateSpring(time);
        return this.initPos.plus(this.getSpringVector());
    }

    act(time) {
        this.pos = this.getNextPosition(time);
    }
}

class Player extends Actor {
    constructor(pos = new Vector()) {
        if (!(pos instanceof Vector)) {
            throw new Error("Player: \'pos\' must be instance of Vector.");
        }
        super(pos.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5));
    }

    get type() { return "player"; }
}

const dict = {
    "@": Player,
    "=": HorizontalFireball,
    "|": VerticalFireball,
    "v": FireRain,
    "o": Coin        
};

const parser = new LevelParser(dict);

loadLevels().then( str => {
    const levels = JSON.parse(str);
    runGame(levels, parser, DOMDisplay)
        .then(() => alert('Вы выиграли!'));

});